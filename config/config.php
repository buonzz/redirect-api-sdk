<?php


return [
    'api_url' => env('REDIRECT_API_URL', 'http://localhost:8000/wp-json/darwinb-testplugin/v1/'),
    'api_user' => env('REDIRECT_API_USER', ''),
    'api_pass' => env('REDIRECT_API_PASS', '')
];