<?php

namespace Darwinb\RedirectApiSdk;

use Illuminate\Support\ServiceProvider;
use Darwinb\RedirectApiSdk\Commands\CallRedirectApi;


class RedirectApiSdkServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('redirect-api-sdk.php'),
            ], 'config');
            
            // Registering package commands.
            $this->commands([
                CallRedirectApi::class
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'redirect-api-sdk');

        // Register the main class to use with the facade
        $this->app->singleton('redirect-api-sdk', function () {
            return new RedirectApiSdk;
        });
    }
}
