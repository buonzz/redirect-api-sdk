<?php

namespace Darwinb\RedirectApiSdk;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Darwinb\RedirectApiSdk\Skeleton\SkeletonClass
 */
class RedirectApiSdkFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'redirect-api-sdk';
    }
}
