<?php

namespace Darwinb\RedirectApiSdk\Models;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $table = 'ras_redirects';

    public function api_call()
    {
        return $this->belongsTo('Darwinb\RedirectApiSdk\Models\ApiCall');
    }
}