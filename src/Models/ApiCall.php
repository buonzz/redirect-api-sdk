<?php

namespace Darwinb\RedirectApiSdk\Models;

use Illuminate\Database\Eloquent\Model;

class ApiCall extends Model
{
    protected $table = 'ras_apicalls';

    public function redirects()
    {
        return $this->hasMany('Darwinb\RedirectApiSdk\Models\Redirect', 'ras_apicalls_id');
    }
}