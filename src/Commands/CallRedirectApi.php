<?php

namespace Darwinb\RedirectApiSdk\Commands;

use Illuminate\Console\Command;
use Darwinb\RedirectApiSdk\Models\ApiCall;
use Darwinb\RedirectApiSdk\Jobs\RetrieveRedirectsFromApi;

class CallRedirectApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redirect_api:call';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initiate call to Redirect API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api_call = new ApiCall();
        $api_call->initiated_at = now();
        $api_call->save();
        
        RetrieveRedirectsFromApi::dispatch($api_call);
    }
}
