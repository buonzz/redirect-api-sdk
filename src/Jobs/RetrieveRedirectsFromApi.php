<?php

namespace Darwinb\RedirectApiSdk\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Client;
use Darwinb\RedirectApiSdk\Models\ApiCall;
use Darwinb\RedirectApiSdk\Models\Redirect;

class RetrieveRedirectsFromApi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $api_call;

    public function __construct(ApiCall $api_call)
    {
        $this->api_call = $api_call;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $client = new Client([
            'base_uri' => config('redirect-api-sdk.api_url'),
            'timeout'  => 30
        ]);

        $response = $client->request('GET', 
                                     'test-redirects',
                                     [
                                         'auth' => ['username', 'password']
                                     ]);
        
        $code = $response->getStatusCode();
        
        $this->api_call->completed_at = now();
        $this->api_call->save();
                                        
        if($code == 200){
            $data = json_decode( (string) $response->getBody());
            if ($data !== null && json_last_error() === JSON_ERROR_NONE) {
                foreach($data as $item){
                    $r = new Redirect();
                    $r->url = $item;
                    $this->api_call->redirects()->save($r);
                }// foreach
             } // if valid data
        } // if http call is ok

    } // handle
}
