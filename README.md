# Redirect Client SDK

Demo Laravel Package for consuming my custom WP-JSON Endpoint.  
**wp-json/darwinb-testplugin/v1/test-redirects**

## Installation

You can install the package via composer.
First add the repo url to your composer.json file

```
{
    "require": {
        "darwinb/redirect-api-sdk": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/buonzz/redirect-api-sdk"
        }
    ]
}

```

fetch it in your vendor folder
```
composer update
```




## Usage

``` php
// Usage description here
```

